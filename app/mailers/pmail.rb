class Pmail < ApplicationMailer
  default from: "nauman.dev@gmail.com"

  def send_email(email,filename)
    attachments["#{filename}.pdf"] = File.read("#{Rails.root}/pdfs/#{filename}.pdf")
    mail(to: email, subject: 'Sample Email')
  end
end
