class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]

  # GET /projects
  # GET /projects.json
  def index
    @projects = Project.all
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
  end

  # GET /projects/new
  def new
    @project = Project.new
    @project.tasks.build
  end

  # GET /projects/1/edit
  def edit
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def view_pdf
    @project = Project.find(params[:id])
    filename = @project.name.strip# + Time.now.to_s
    render  :pdf => filename  ,
            #:template => 'layouts/pdf.pdf.erb',    to render any partial/view >> if commented,function name view wud be called
            :layout => 'layouts/pdf.pdf.erb',
            :wkhtmltopdf => '/home/naumantahir/.rbenv/shims/wkhtmltopdf',
            :save_to_file => Rails.root.join('pdfs', "#{filename}.pdf")
            #save_only:                      false,                        # depends on :save_to_file being set first
    Pmail.send_email('nomibzu@gmail.com',filename).deliver
    #send_file '/path/to.zip'
    #attachments['filename.jpg'] = File.read('/path/to/filename.jpg')
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:name, tasks_attributes: [:id, :name, :description, :_destroy])
    end
end
